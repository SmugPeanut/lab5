#include <iostream>
using namespace std;

void printArray(int array[], int size);

int main(){

  int numbers[] = {2, 4, 8, 16, 32};
  int size = sizeof(numbers)/sizeof(numbers[0]);

  printArray(numbers, size);
}

void printArray(int array[], int size){

  for (int i = 0; i < size; i++){

    if (i == 0){
      cout << "[";
    }

    cout << array[i] << ", ";

    if (i == (size-1)){
      cout << "\b\b ]";
    }
  }

}

#include <iostream>
using namespace std;

void swap(int array[], int first, int second);
void printArray(int array[], int size);

int main (){

  int numbers[] = {1, 2, 3, 4, 5};
  int size = sizeof(numbers)/sizeof(numbers[0]);
  
  int firstVal, secondVal;
  cout << "Enter the indices you want to swap: ";
  cin >> firstVal;
  cin >> secondVal;
  while (firstVal > size || secondVal > size){
    cout << "Make sure your values are smaller than the size " << size << "of the array";
    cin >> firstVal, secondVal;
  }

  cout << "array before swap: ";
  printArray(numbers, size);

  swap(numbers, firstVal, secondVal);

  cout << "array after swap: ";

  printArray(numbers, size);
}

void swap(int array[], int first, int second){    //swap function

  int temp;

  temp = array[first];
  array[first] = array[second];
  array[second] = temp;
  
}

void printArray(int array[], int size){

  for (int i = 0; i < size; i++){

    if (i == 0){
      cout << "[";
    }

    cout << array[i] << ", ";

    if (i == (size-1)){
      cout << "\b\b ]";
    }
  }

}

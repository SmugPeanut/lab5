#include <iostream>
using namespace std;

int cube (int);

int main(){

  int myVal;
  cout << "Enter an integer value: ";
  cin >> myVal;
  cout << cube(myVal);

}

int cube(int x){

  int cubed = x*x*x;
  return cubed;

}
